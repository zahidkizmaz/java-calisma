public class Yemek {

    private String yemekIsmi;
    private double fiyat;

    public Yemek(String yemekIsmi, double fiyat) {
        this.yemekIsmi = yemekIsmi;
        this.fiyat = fiyat;
    }

    public String getYemekIsmi() {
        return yemekIsmi;
    }

    public void setYemekIsmi(String yemekIsmi) {
        this.yemekIsmi = yemekIsmi;
    }

    public double getFiyat() {
        return fiyat;
    }

    public void setFiyat(double fiyat) {
        this.fiyat = fiyat;
    }

    @Override
    public String toString() {
        return  "YemekIsmi=" + yemekIsmi +
                ", Fiyat=" + fiyat + "\n";
    }
}
