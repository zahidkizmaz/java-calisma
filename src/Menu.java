import java.util.List;

public class Menu {

    private List<Yemek> yemekler;

    public Menu(List<Yemek> yemekler) {
        this.yemekler = yemekler;
    }

    public void setYemekler(List<Yemek> yemekler) {
        this.yemekler = yemekler;
    }

    public List<Yemek> getYemekler() {
        return yemekler;
    }

    @Override
    public String toString() {
        StringBuilder yemkeList = new StringBuilder();
        for (Yemek y: yemekler) {
            yemkeList.append(y);
        }
        return yemkeList.toString();
    }
}
