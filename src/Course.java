
import java.util.List;

public class Course {

    private int id;
    private String courseName;
    private String instName;

    public Course() {

    }

    public Course(int id, String courseName, String instName) {
        this.id = id;
        this.courseName = courseName;
        this.instName = instName;
    }

    public void histogram(List<Double> notlar){
        int[] frekanslar = new int[10];

        for (Double not : notlar) {
            if (not < 9 && not > 0){
                frekanslar[0]++;
            }
            else if(not > 9 && not <20){
                frekanslar[1]++;
            }
            else if(not > 19 && not <30){
                frekanslar[2]++;
            }
        }

        for (int i = 0; i < 3; i++) {
            System.out.println(new Frekans(frekanslar[i], "Frekans:" + i) );
            System.out.println();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getInstName() {
        return instName;
    }

    public void setInstName(String instName) {
        this.instName = instName;
    }
}
