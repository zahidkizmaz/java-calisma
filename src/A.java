public class A {

    private String name;
    private String sname;

    public static int id = 10;

    public A() {

        this("hatice", "erdirik");
    }

    private A(String name, String sname) {
        this.name = name;
        this.sname = sname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
}
