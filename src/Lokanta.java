public class Lokanta {

    private String ad, adres, sahibi;
    private Menu m;
    private int bosMasa, doluMasa;

    public Lokanta(String ad, Menu m, int bosMasa, int doluMasa) {
        this.ad = ad;
        this.m = m;
        this.bosMasa = bosMasa;
        this.doluMasa = doluMasa;
    }

    public boolean yerAyirt(int masaSayisi){
        if (this.bosMasa >= masaSayisi) {
            this.bosMasa -= masaSayisi;
            this.doluMasa += masaSayisi;
            System.out.println("Masanız tutulmuştur.");
            return true;
        }else{
            System.out.println("Yeterli masamız yoktur. ");
            return false;
        }
    }

    public void menuyuGoster(){
        System.out.println(this.ad + " Menusu:");
        System.out.println(this.m);
    }

    public int getBosMasa() {
        return bosMasa;
    }

    public int getDoluMasa() {
        return doluMasa;
    }
}
