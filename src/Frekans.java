public class Frekans {

    private int star;
    private String aralik;

    public Frekans(int star, String aralik) {
        this.star = star;
        this.aralik = aralik;
    }

    @Override
    public String toString() {
        StringBuilder fr = new StringBuilder();
        for (int i = 0; i < star; i++) {
            fr.append(" * \n");
        }
        fr.append("____\n\n");
        fr.append(this.aralik);
        return fr.toString();
    }
}
